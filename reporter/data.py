"""Data format to be passed to the template."""
from itertools import chain
from cached_property import cached_property
from . import settings

# This is a temporary workaround while there's no easy way to retrieve all
# data via .list(), should be replaced with 'as_list=False' or similar once
# implemented
MAX_LIMIT = 100


class BuildTestData:
    """Data about single build's tests."""

    def __init__(self, build):
        """Initialize."""
        self.build = build

    def _tests_filtered(self, condition, exclude_waived=False):
        tests = self.tests
        if exclude_waived:
            tests = filter(lambda x: not x.waived, tests)
        return list(filter(condition, tests))

    @cached_property
    def tests(self):
        """Get a list of build's tests."""
        return self.build.tests.list(limit=MAX_LIMIT)

    @cached_property
    def passed_tests(self):
        """Get a list of build's tests that passed."""
        return self._tests_filtered(lambda test: test.status == 'PASS')

    @cached_property
    def skipped_tests(self):
        """Get a list of build's tests that got skipped."""
        return self._tests_filtered(lambda test: test.status == 'SKIP')

    @cached_property
    def failed_tests(self):
        """Get a list of build's tests that failed."""
        return self._tests_filtered(lambda test: test.status == 'FAIL',
                                    exclude_waived=True)

    @cached_property
    def errored_tests(self):
        """Get a list of build's tests that encountered an infra error."""
        return self._tests_filtered(lambda test: test.status == 'ERROR')

    @cached_property
    def waived_tests(self):
        """Get a list of waived build's tests."""
        return self._tests_filtered(lambda test: test.waived)


class RevisionData:
    """Data about a single revision."""

    def __init__(self, revision_id):
        """Initialize."""
        self.rev = settings.DATAWAREHOUSE.kcidb.revisions.get(id=revision_id)

    @cached_property
    def builds(self):
        """Return a list of revision's builds."""
        return self.rev.builds.list(limit=MAX_LIMIT)

    @cached_property
    def passed_builds(self):
        """Return passing builds."""
        return list(filter(lambda x: x.valid, self.builds))

    @cached_property
    def failed_builds(self):
        """Return failing builds."""
        return list(filter(lambda x: not x.valid, self.builds))

    @cached_property
    def build_test_data(self):
        """Get test data per build."""
        return {build: BuildTestData(build) for build in self.builds}

    @cached_property
    def all_test_sets(self):
        """Return all revision's tests."""
        return self.build_test_data.values()

    def __getattr__(self, name):
        """Get attribute with BuildTestData aggregations."""
        # Aggregate for BuildTestData properties
        if name.endswith('tests') and name in BuildTestData.__dict__:
            data = [getattr(test_set, name) for test_set in self.all_test_sets]
            return list(chain.from_iterable(data))
        try:
            return self.__dict__[name]
        except KeyError:
            msg = "'{0}' object has no attribute '{1}'"
            raise AttributeError(msg.format(type(self).__name__,
                                            name)) from None

    @cached_property
    def result(self):
        """
        Get the overall result.

        True if everything passed, False otherwise.
        """
        return (self.rev.valid and self.failed_builds == [] and
                self.failed_tests == [])
