"""Main reporter definition."""
import os
from argparse import ArgumentParser
import sentry_sdk
from cki_lib import misc
from . import settings
from .data import RevisionData
from .utils import status_to_emoji, yesno


settings.JINJA_ENV.filters['status_to_emoji'] = status_to_emoji
settings.JINJA_ENV.filters['yesno'] = yesno


class ReporterException(Exception):
    """An exception in reporting."""


def render_template(revision_id):
    """Render the report template using revision data."""
    revision_data = RevisionData(revision_id)
    template = settings.JINJA_ENV.get_template('report.j2')
    return template.render(revision_data=revision_data)


# pylint: disable=unused-argument
def process_message(routing_key, payload):
    """Process the webhook message."""
    settings.LOGGER.info('processing message (payload): %s', payload)
    if payload['status'] != 'ready_to_report':
        settings.LOGGER.info('Skipping status: "%s"', payload['status'])
        return
    if 'object_type' in payload and payload['object_type'] == 'revision' and \
       'id' in payload:
        print(render_template(payload['id']))
    else:
        error = 'Invalid "ready_to_report" message recieved'
        settings.LOGGER.error(error)
        raise ReporterException(error)


def create_parser():
    """Create argument parser."""
    parser = ArgumentParser('Create reports for provided pipelines')
    group = parser.add_mutually_exclusive_group(required=False)
    group.add_argument('-l', '--listen', action='store_true',
                       help='Start polling for rabbitmq messages and create ' +
                       'reports for ready_to_report revisions.' +
                       'Defaults to this if no other option was selected.')
    group.add_argument('-r', '--revision_id', type=str,
                       help='Specify the revision id of a revision ' +
                       'you want to create a single report for.')
    return parser


def main():
    """Set up and start consuming messages."""
    if misc.is_production():
        sentry_sdk.init(ca_certs=os.getenv('REQUESTS_CA_BUNDLE'))
    parser = create_parser()
    args = parser.parse_args()

    if args.revision_id:
        print(render_template(args.revision_id))
    # Default to rabbitmq consuming
    else:
        print('Now consuming Rabbitmq messages')
        settings.QUEUE.consume_messages(
            settings.REPORTER_EXCHANGE,
            ['#'],
            process_message,
            queue_name=settings.REPORTER_QUEUE
        )


if __name__ == '__main__':
    main()
