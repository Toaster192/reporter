"""Initialize the message queue."""
import os
from pathlib import Path

from jinja2 import Environment, FileSystemLoader
from cki_lib import misc
from cki_lib.logger import get_logger
from cki_lib.messagequeue import MessageQueue
from datawarehouse import Datawarehouse

RABBITMQ_HOST = os.environ.get('RABBITMQ_HOST', 'localhost')
RABBITMQ_PORT = misc.get_env_int('RABBITMQ_PORT', 5672)
RABBITMQ_USER = os.environ.get('RABBITMQ_USER', 'guest')
RABBITMQ_PASSWORD = os.environ.get('RABBITMQ_PASSWORD', 'guest')

REPORTER_EXCHANGE = os.environ.get('REPORTER_EXCHANGE',
                                   'cki.exchange.datawarehouse.kcidb')
REPORTER_QUEUE = os.environ.get('REPORTER_QUEUE',
                                'cki.queue.datawarehouse.kcidb.reporter')

QUEUE = MessageQueue(RABBITMQ_HOST, RABBITMQ_PORT,
                     RABBITMQ_USER, RABBITMQ_PASSWORD)

DATAWAREHOUSE_HOST = os.environ.get('DATAWAREHOUSE_HOST', 'http://localhost')
DATAWAREHOUSE_TOKEN = os.environ.get('DATAWAREHOUSE_TOKEN')

DATAWAREHOUSE = Datawarehouse(DATAWAREHOUSE_HOST, token=DATAWAREHOUSE_TOKEN)

LOGGER = get_logger('cki.reporter')

TEMPLATE_DIR = Path(__file__).resolve().parent / 'templates'

# Set up the jinja2 environment which can be reused throughout this script.
JINJA_ENV = Environment(
    loader=FileSystemLoader(TEMPLATE_DIR),
    trim_blocks=True,  # Remove first newline after a jinja2 block
    lstrip_blocks=True,  # Strip whitespace from the left side of tags
)
