from unittest import TestCase
from unittest.mock import patch
from reporter.data import RevisionData, BuildTestData
from .mocks import get_mocked_revision, get_mocked_build, get_mocked_test


class TestData(TestCase):
    """Tests for reporter/data.py ."""

    @patch('reporter.settings.DATAWAREHOUSE.kcidb.revisions.get')
    def test_revision_data(self, revisions_get_mock):
        """Test the RevisionData class and it's properties."""
        tests = [get_mocked_test('PASS', False)]
        builds = [get_mocked_build(True, tests)]
        revision = get_mocked_revision(True, builds)

        revision_id = '123'
        revisions_get_mock.return_value = revision
        revision_data = RevisionData(revision_id)
        revisions_get_mock.assert_called_with(id=revision_id)

        self.assertEqual(revision_data.rev, revision)

        self.assertEqual(revision_data.builds, builds)

        self.assertEqual(revision_data.passed_builds, builds)

        self.assertEqual(len(revision_data.all_test_sets), 1)
        self.assertEqual(list(revision_data.all_test_sets)[0].tests, tests)
        self.assertTrue(revision_data.passed_tests)

        self.assertTrue(revision_data.result)

        with self.assertRaises(AttributeError):
            revision_data.foo

        with self.assertRaises(AttributeError):
            revision_data.tests_foo

    @patch('reporter.settings.DATAWAREHOUSE.kcidb.revisions.get')
    def test_revision_data_failed_build_result(self, revisions_get_mock):
        """Test the RevisionData result property."""
        tests = []
        builds = [get_mocked_build(True, tests),
                  get_mocked_build(False, tests)]
        revision = get_mocked_revision(True, builds)

        revisions_get_mock.return_value = revision
        revision_data = RevisionData('1234')

        revision_data = RevisionData(revision)

        self.assertFalse(revision_data.result)

    def test_build_test_data(self):
        """Test the RevisionData class and it's properties."""
        pass_test = get_mocked_test('PASS', False)
        pass_waived_test = get_mocked_test('PASS', True)
        fail_test = get_mocked_test('FAIL', False)
        fail_waived_test = get_mocked_test('FAIL', True)
        error_test = get_mocked_test('ERROR', False)
        skip_test = get_mocked_test('SKIP', False)
        tests = [pass_test, fail_test, pass_waived_test, fail_waived_test,
                 error_test, skip_test]
        build = get_mocked_build(True, tests)

        build_test_data = BuildTestData(build)

        self.assertEqual(build_test_data.tests, tests)

        self.assertEqual(build_test_data.failed_tests, [fail_test])
        self.assertEqual(build_test_data.passed_tests, [pass_test,
                                                        pass_waived_test])
        self.assertEqual(build_test_data.skipped_tests, [skip_test])
        self.assertEqual(build_test_data.errored_tests, [error_test])
        self.assertEqual(build_test_data.waived_tests, [pass_waived_test,
                                                        fail_waived_test])
