"""Test mocks."""
from unittest.mock import MagicMock


def get_mocked_revision(valid=True, builds=None):
    """Return a kcidb revision mock."""
    revision = MagicMock()
    revision.valid = valid

    revision.builds.list.return_value = builds if builds is not None else []
    return revision


def get_mocked_build(valid=True, tests=None):
    """Return a kcidb build mock."""
    build = MagicMock()
    build.valid = valid

    build.tests.list.return_value = tests if tests is not None else []
    return build


def get_mocked_test(status='PASSED', waived=False):
    """Return a kcidb test mock."""
    test = MagicMock()
    test.status = status
    test.waived = waived

    return test
