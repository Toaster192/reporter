from unittest import TestCase
from reporter.utils import yesno, status_to_emoji


class TestUtils(TestCase):
    """Tests for reporter/utils.py ."""

    def test_yesno(self):
        """Test yesno filter/function."""
        assert yesno(True, 'yeah,no,maybe') == 'yeah'
        assert yesno(False, 'yeah,no,maybe') == 'no'
        assert yesno(None, 'yeah,no,maybe') == 'maybe'
        assert yesno(True, 'yes,no') == 'yes'
        assert yesno(False, 'yes,no') == 'no'
        assert yesno(True) == 'yes'
        assert yesno(False) == 'no'

    def test_status_to_emoji(self):
        """Test status_to_emoji filter/function."""
        # Not testing 'SKIP' as the filter should
        # never be called with a skipped test
        statuses = ['PASS', 'DONE', 'FAIL', 'ERROR']
        expected_emoji = ['✅', '✅', '❌', '⚡⚡⚡']
        for status, emoji in zip(statuses, expected_emoji):
            assert status_to_emoji(status) == emoji
